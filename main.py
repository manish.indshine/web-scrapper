import requests
from bs4 import BeautifulSoup
import os
import sys
import pandas as pd
import logging


def get_logger(name):
    return logging.getLogger(name)


def log2file(file_name):  # Loggging to file
    # set up logging to file
    logging.basicConfig(
        filename=os.path.join(file_name + "_log.log"),
        level=logging.DEBUG,
        format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
        datefmt='%H:%M:%S'
    )

    # set up logging to console
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    logger = logging.getLogger(__name__)

# Searching for tags and classes


def findit(web, _tag, _class):
    page = requests.get(web)

    if page.status_code == 200:
        print('successfully downloaded %s' % (web))
        logger.debug('successfully downloaded %s' % (web))

    else:
        print('coundn"t download the website. Error code is ' +
              str(page.status_code))

        logger.debug('coundn"t download the website. Error code is ' +
                     str(page.status_code))

    soup = BeautifulSoup(page.content, 'html.parser')

    # Finding all the GIS comapnies list
    a = list(soup.find_all(_tag, class_=_class))
    return a


# Get main website
def get_main(web, _tag, _class):

    a = findit(web, _tag, _class)
    city = []

    for i in a:
        dicti = i.attrs
        if len(str(dicti['href']).split('/directory/gis-companies-in-india')) == 1:
            continue
        else:
            str1, str2 = str(dicti['href']).split(
                '/directory/gis-companies-in-india')
            city.append('/directory/gis-companies-in-india' + str2)

    return city


# Get children websites
def get_children(web, _tag, _class):
    a = []
    counter = 1
    for j in web:
        a.append(list(findit(j, _tag, _class)))
        print('completed %s out of %s' % (counter, len(web)))

        logger.debug('completed %s out of %s' % (counter, len(web)))

        counter = counter + 1
    return a


# Main loop starts
def main():
    city = get_main(web, 'a', 'sites-navigation-link')

    city_link = []
    for i in range(len(city)):
        city_link.append('http://www.gisinindia.com' + city[i])

    print('Number of links found are %s' % (len(city_link)))
    logger.debug('Number of links found are %s' % (len(city_link)))

    children = get_children(city_link, 'tr', 'goog-ws-list-tableRow')

    name = []
    website = []
    description = []
    contact = []

    for i in range(len(children)):
        companies = children[i]
        for j in range(len(companies)):
            href_tags = companies[j].find(href=True)

            if href_tags:
                website.append(href_tags['href'])
            else:
                website.append('not found')

            name.append(companies[j].find(
                class_="goog-ws-list-url").get_text())
            info = list(companies[j].find_all('td'))
            description.append(info[1].get_text())
            contact.append(info[2].get_text())

    data = [name, website, description, contact]
    df = pd.DataFrame(data)
    df1 = df.transpose()

    df1.to_csv(file_name + '.csv', sep=',', encoding='utf-8')


if __name__ == '__main__':
    try:
        logger = get_logger('scrapping')
        file_name = 'companies'

        web = 'http://www.gisinindia.com/directory/gis-companies-in-india'
        main()
        sys.exit()

    except KeyboardInterrupt:
        print('Interrupted')
        logger.debug('Interrupted')

        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
            
    log2file(file_name)
